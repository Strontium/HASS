import System.Environment (getArgs)
import Data.Char (isSpace)

main :: IO ()
main = do n <- getArgs
          x <- readFile $ n !! 0
          putStrLn . unlines $ map (addBraces . addColons . addSemicolons) (preProcess . lines $ x)

addColons :: String -> String
addColons x
    | isSpace $ head x = " " ++ first x ++ ": " ++ rest x
    | otherwise        = x
    where first = head . words
          rest  = unwords . tail . words

addSemicolons :: String -> String
addSemicolons x
    | isSpace $ head x = x ++ ";"
    | otherwise        = x

addBraces :: String -> String
addBraces x
    | x == "."          = "}"
    | notSpace $ head x = x ++ " {"
    | otherwise         = x
    where notSpace = not . isSpace


preProcess :: [String] -> [String]
preProcess x = [ y | y <- x, y /= "" ]

